# openSUSE pape maker

A script for an obnoxious ~~arch~~ openSUSE user who wants to trumpet his (or her) distro
of choice.

Use this script to place the ~~arch~~ openSUSE logo on all your wallpapers.

## I use ~~arch~~ openSUSE btw...

# Usage

```
./mepapemaker.sh file.img
```

# Examples

## Example 1
![example1](examples/example1.png)
![arch btw example1](examples/openSUSE_btw_example1.png)

# Dependencies

```Imagemagick```
